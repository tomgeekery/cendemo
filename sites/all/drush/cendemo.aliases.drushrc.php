<?php

$aliases['dev-ckeller'] = array(
  'uri' => 'cendemo.dev',
  'root' => '/home/ckeller/htdocs/cendemo',
  'path-aliases' => array(
    '%files' => 'sites/default/files',
  ),
);

$aliases['stage'] = array(
  'uri' => 'stagecendemo.dev',
  'root' => '/home/ckeller/htdocs/stagecendemo',
  'path-aliases' => array(
    '%files' => 'sites/default/files',
  ),
);

$aliases['prod'] = array(
  'uri' => 'example.com',
  'remote-host' => 'host.example.com',
  'remote-user' => 'example',
  'root' => '/home/example/public_html',
  'path-aliases' => array(
    '%files' => 'sites/default/files',
  ),
);
